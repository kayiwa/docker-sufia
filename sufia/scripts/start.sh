# Not using this script right now
#!/usr/bin/env bash

echo "Installing bundler"
gem install bundler

echo "kludging nokogiri install error"
bundle config build.nokogiri --use-system-libraries

echo "Bundling gems"
bundle install --jobs 4 --retry 3

echo "Not sure how specify install path in rails generate so ..."
rails new /opt/druw 
mv druw.Gemfile Gemfile
bundle install

echo "Installing sufia"
rails generate sufia:install -f
rake db:migrate RAILS_ENV=development

echo "Editing solr config"

#echo "Generating Spring binstubs"
#bundle exec spring binstub --all
 
echo "Clearing logs"
rake log:clear
 
#echo "Setting up new db if one doesn't exist"
#rake db:version || { bundle exec rake db:setup; }
 
echo "Removing contents of tmp dirs"
rake tmp:clear
 
echo "Starting app server"
bundle exec rails s -p 3000
