#Fedora4, Sufia, docker recipe. 
Works. More or less. 

## Reference
https://github.com/curationexperts/hydradam/wiki/Production-Installation%3A-Overview

https://github.com/projecthydra/sufia

http://www.talkingquickly.co.uk/2014/06/rails-development-environment-with-vagrant-and-docker/

containers-dev

 - hydrabase
 - tomcat/fedora4/solr
 - redis
 - sufia

####
#Development

## Clone this repo
`$ git clone git@bitbucket.org:uwlib/docker-sufia.git`

## Set environment variables on host machine
`$ export PROJDIR=/some/path/to/Dockerfiles (this cloned repo)`

`$ export HYDRA_NAME="druw"`

## Clone a vanilla sufa repo (druw) into $PROJDIR
`$ git clone git@bitbucket.org:uwlib/druw.git`

### Copy and edit the config files in druw.
    cp config/database.yml.template config/database.yml
    cp config/fedora.yml.template config/fedora.yml
    cp config/solr.yml.template config/solr.yml
    cp config/redis.yml.template config/redis.yml

* Instead of 'localhost' use the docker alias in the link.
    Eg. 
    development:
       url: http://tomcat:8080/druw

* The port is the port of the container, not the mapped port on the host. Sheesh.     

## Build a centos7 image named druw/hydrabase which contains build tools
`$ sudo docker build -t druw/hydrabase hydrabase/.`

## Build a tomcat/fedora4/solr image 
`$ cd $projdir/tomcat`

`$ sudo docker build -t tomcat:f4solr .` 

* druw.xml contains location information for rails app -- change accordingly.
* I fudged this and snagged schema.xml, solrconfig.xml from druw repo since mounting those files at run doesn't actually work.

## Start a tomcat/fedora4/solr container.
`$ sudo docker run -d -p 127.0.0.1:8082:8080 --name tomcat tomcat:f4solr`

The following was what I hoped would work, but doesn't. Leaving it here in case someone can figure a better way to get schema.xml and solrconfig.xml into this tomcat container

`$ sudo docker run -d -p 127.0.0.1:8082:8080 -v $PROJDIR/$HYDRA_NAME/solr_conf/conf/schema.xml:/opt/solr/$HYDRA_NAME/collection1/conf/schema.xml -v $PROJDIR/$HYDRA_NAME/solr_conf/conf/solrconfig.xml:/opt/solr/$HYDRA_NAME/collection1/conf/solrconfig.xml --name tomcat tomcat:f4solr`

* If you name the tomcat container something else, don't forget to change it in solr.yml and fedora.yml
* Check if this is working by going to http://localhost:8082/fedora and http://localhost:8082/druw on host machine.

## Start a redis container
`$ sudo docker run --name redis -d redis`

* If you name the redis container something else, don't forget to change it in redis.yml.

## Build a sufia image 
`$ cd $PROJDIR/sufia`

`$ sudo docker build -t druw .`

## Start a temporary sufia container linked to tomcat, redis containers, with druw/ dir mounted as a shared volume to bundle the gems.

`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --rm druw:latest bash -c "cd /opt/druw && bundle install --path vendor/bundle"`

* --privileged flag is needed if SELinux is running. 
* --path flag is needed otherwise I have no idea where bundle is installing the gems.

## Create a secrets.yml file on the shared volume
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --rm druw:latest bash -c "cd /opt/druw && bundle exec rake secret > /opt/druw/config/secrets.yml"`

* Probably just have to edit secrets.yml by hand after so it looks like the following
    development:
       secret_key_base: $THATBIGSTRING`

## Run rake db on the shared volume
`sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --rm druw:latest bash -c "cd /opt/druw && bundle exec rake db:migrate"`

* RAILS_ENV was set to 'development' in the sufia Dockerfile.

## Finally, start this thing
`sudo docker run --privileged=true -d -p 127.0.0.1:3300:3000 -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis druw:latest bash -c "cd /opt/druw && bundle exec resque-pool --daemon --environment development start && /ruby_gems/2.1/bin/rails server"`

* resque creates a pid in host:$PROJDIR/druw/tmp/pids/
* sudo rm that if you want to kill it.
* To kill rails server, stop the container and remove it.
* Then remove the pid from host:$PROJDIR/druw/tmp/pids/  -- it it hasn't removed it already.

## Scripts
the bash scripts in scripts/ dir work more or less.

`$ sh $PROJDIR/scripts/rebuild_app.sh` tomcat and redis need to still be running.

`$ sh $PROJDIR/scripts/start_rails.sh` starts resque workers and rails server

`$ sh $PROJDIR/scripts/stop_rails.sh` stops rails server and resque workers, then gets rid of PIDs.

## Notes

 - Sufia doesn't like it if development and production URLs are the same in database.yml
 - Only run the following once `rails generate sufia:install -f` or weird things happen.
 - So don't run that if you are cloning druw repo.

## Troubleshoot docker build
 - error: "Cannot find a valid baseurl for repo: base/7/x86_64" when running docker build on a Clearwire hotspot.
 - This took me forever to figure out so here's the link http://stackoverflow.com/questions/25130536/dockerfile-docker-build-cant-download-packages-centos-yum-debian-ubuntu-ap
 - on fedora21 that's in /etc/sysconfig/docker-network
