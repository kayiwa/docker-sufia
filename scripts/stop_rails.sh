#! /usr/bin/env bash

# Stop the most recently started container, wait for it to terminate, then
# remove all stopped containers.

stopcontainer=$(sudo docker ps -l -q)
sudo docker stop $stopcontainer
sudo docker wait $stopcontainer
sudo docker rm $(sudo docker ps -a -q)
sudo rm $PROJDIR/druw/tmp/pids/*
