#! /usr/bin/env bash

sudo docker run --privileged=true -d -p 127.0.0.1:3300:3000 -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis druw:latest bash -c "cd /opt/druw && bundle exec resque-pool --daemon --environment development start && /usr/local/bin/rails server"
