#! /usr/bin/env

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --rm druw:latest bash -c "cd /opt/druw && bundle exec rake assets:clean"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --rm druw:latest bash -c "cd /opt/druw && bundle exec rake assets:clobber"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --rm druw:latest bash -c "cd /opt/druw && bundle install --path vendor/bundle"

wait $!

sudo docker run --privileged=true -i -t -v $PROJDIR/druw:/opt/druw --link tomcat:tomcat --link redis:redis --rm druw:latest bash -c "cd /opt/druw && bundle exec rake db:migrate"
